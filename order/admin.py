from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from django.utils.html import format_html_join
from django.utils.safestring import mark_safe
from order.models import *


# Register your models here.



class FoodAdmin(ModelAdmin):
	list_display = ['food_name', 'food_availability', 'food_image', 'food_price']


class FoodOrderAdmin(ModelAdmin):
	search_fields = ['customer_']
	list_filter = ['order_time']
	list_display = ['food_', 'quantity', 'customer_']


class PaymentAdmin(ModelAdmin):
	search_fields = ['customer_name']
	list_display = ['customer_name', 'order', 'quantity', 'total', 'payed', 'received']

	def order(self, instance):
		return format_html_join(
			mark_safe(
				'<br/>'
			), '<li style="font: 12px/1.5 Helvetica, sans-serif;padding-left: 60px;color: #555;">{}</li>', ((food_name,) for food_name in instance.order()),
		) or mark_safe("<span class='errors'>I can't determine this address.</span>")

	def quantity(self, instance):
		return format_html_join(
			mark_safe(
				'<br/>'
			), '<li>{}</li>', ((quantity,) for quantity in instance.quantity()),
		) or mark_safe("<span class='errors'>I can't determine this address.</span>")


admin.site.register(Food, FoodAdmin)
admin.site.register(FoodOrder, FoodOrderAdmin)
admin.site.register(Payment, PaymentAdmin)
