__author__ = 'peter'

from django import forms

"""
	our customer form that
	is been showed at the index page
"""


class CustomerForm(forms.Form):
	tel_number = forms.CharField()

	class Meta:
		widgets = {
			'tel_number': forms.NumberInput(attrs=
			                                {'class': 'validate',
			                                 'number': 'text',
			                                 'id': 'tel_number',
			                                 'placeholder': 'Enter Phone Number'
			                                 })
		}


class PaymentForm(forms.Form):
	card_name = forms.CharField()
	card_number = forms.CharField()

	class Meta:
		widget = {
			'card_name': forms.TextInput(attrs={'class': 'validate', 'type': 'text', 'id': 'card_name'}),
			'card_number': forms.TextInput(attrs={'class': 'validate', 'type': 'number', 'id': 'card_number'})
		}
