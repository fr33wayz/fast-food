# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Food',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('food_name', models.CharField(max_length=255)),
                ('food_price', models.IntegerField(default=50)),
                ('food_time_duration', models.TimeField()),
                ('food_image', models.ImageField(upload_to=b'uploads', blank=True)),
                ('food_availability', models.BooleanField(default=True)),
            ],
        ),
    ]
