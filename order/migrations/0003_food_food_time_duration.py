# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_remove_food_food_time_duration'),
    ]

    operations = [
        migrations.AddField(
            model_name='food',
            name='food_time_duration',
            field=models.TimeField(default=datetime.datetime(2015, 11, 6, 18, 43, 52, 832467, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
