# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_food_food_time_duration'),
    ]

    operations = [
        migrations.AddField(
            model_name='food',
            name='food_discount',
            field=models.CharField(max_length=222, null=True, blank=True),
        ),
    ]
