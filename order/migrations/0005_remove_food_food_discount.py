# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_food_food_discount'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='food',
            name='food_discount',
        ),
    ]
