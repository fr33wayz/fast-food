# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0007_auto_20151219_2329'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cart',
            old_name='order_date',
            new_name='order_time',
        ),
        migrations.AddField(
            model_name='foodorder',
            name='order_time',
            field=models.DateTimeField(default=datetime.datetime(2015, 12, 22, 22, 52, 31, 823681, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
