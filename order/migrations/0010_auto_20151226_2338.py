# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0009_auto_20151222_2303'),
    ]

    operations = [

        migrations.AlterModelOptions(
            name='food',
            options={'verbose_name': 'Meal', 'verbose_name_plural': "Meal's"},
        ),
        migrations.AlterModelOptions(
            name='foodorder',
            options={'ordering': ['-order_time'], 'verbose_name': 'Order', 'verbose_name_plural': "Order's"},
        ),
    ]
