from time import timezone
from django.contrib.auth.models import User
from django.db import models

from string import join
# Create your models here.
from django.utils.html import strip_tags, format_html
from django.utils.safestring import SafeUnicode

"""
our food class that represent the food table
in our db
"""


class Food(models.Model):
	food_name = models.CharField(max_length=255)
	food_price = models.IntegerField(default=50)
	food_time_duration = models.TimeField()
	food_image = models.ImageField(upload_to='uploads', blank=True)
	food_availability = models.BooleanField(default=True)

	class Meta:
		verbose_name = "Meal"
		verbose_name_plural = "Meal's"

	def __unicode__(self):
		return self.food_name


"""
our customer class that represent the
customer table in our db it has the user has
a foreign key so each customer is a user when created
"""


class Customer(models.Model):
	user = models.ForeignKey(User)
	tel_number = models.CharField(max_length=13)
	location = models.CharField(max_length=255, blank=True, null=True)


"""
the cart model repersent the cart table
on our db and has a foreign key of the customer
to the Customer table
"""


class Cart(models.Model):
	customer = models.OneToOneField(Customer)
	active = models.BooleanField(default=True)
	order_time = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = "Customer Order"
		verbose_name_plural = "Customer Orders"

	# a helper method to add a food to the cart
	def add_to_cart(self, food_id):
		food = Food.objects.get(pk=food_id)
		try:
			preexisting_order = FoodOrder.objects.get(food=food, cart=self)
			preexisting_order.quantity += 1
			preexisting_order.save()
		except FoodOrder.DoesNotExist:
			new_order = FoodOrder.objects.create(
				food=food,
				cart=self,
				quantity=1
			)

			new_order.save()

	# helper method to remove a food from the cart
	def remove_from_cart(self, food_id):
		food_to_remove = Food.objects.get(pk=food_id)
		try:
			preexisting_order = FoodOrder.objects.get(food=food_to_remove, cart=self)

			if preexisting_order.quantity > 1:
				preexisting_order.quantity -= 1
				preexisting_order.save()
			else:
				preexisting_order.delete()

		except FoodOrder.DoesNotExist:
			pass


"""
the food order class that represent the
food order table, it has a foreign key
relationship to the Food, Cart
"""


class FoodOrder(models.Model):
	food = models.ForeignKey(Food)
	cart = models.ForeignKey(Cart)
	quantity = models.IntegerField()
	order_time = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return "Food Order"

	class Meta:
		verbose_name = "Ordered Food"
		verbose_name_plural = "Ordered Foods List"
		ordering = ["-order_time"]

	def food_(self):
		return self.food.food_name

	def customer_(self):
		return self.cart.customer.tel_number


class Payment(models.Model):
	cart = models.ForeignKey(Cart)
	total = models.FloatField()
	payed = models.BooleanField(default=False)
	card_name = models.CharField(max_length=255)
	card_number = models.CharField(max_length=19, default='XXXX-XXXX-XXXX-XXXX')
	received = models.BooleanField(default=False)

	class Meta:
		verbose_name = "Customer Payment"
		verbose_name_plural = "Customer Payments"

	def customer_name(self):
		return self.cart.customer.tel_number

	def order(self):
		orders = FoodOrder.objects.filter(cart=self.cart)
		order_list = []
		for order in orders:
			order_list.append(order.food.food_name)
		return order_list

	order.allow_tags = True

	def quantity(self):
		orders = FoodOrder.objects.filter(cart=self.cart)
		order_list = []
		for order in orders:
			order_list.append(order.quantity)
		return order_list

	quantity.allow_tags = True
