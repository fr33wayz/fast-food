from  django.conf.urls import url, patterns
from order.views import *

urlpatterns = patterns(
	'',
	url(r'^$', view=index_page, name='index'),
	url(r'^menu', view=FoodMenuView.as_view(), name='menu'),
	url(r'^add-item/$', view=add_item_to_cart, name='add'),
	url(r'^remove-item/$', view=remove_food_from_cart, name='remove'),
	url(r'^cart/$', view=get_food_from_cart, name='cart'),
	url(r'^logout/$', view=user_logout, name='logout'),
	url(r'^payment-successful/$', view=success_full_payment, name='payment-successful'),
)
