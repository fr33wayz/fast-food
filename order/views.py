from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from models import *
from forms import *
from  django.views import generic
from django.conf.urls import url

"""
this view is reponsible for getting all the
available food and present it to the html template
food_list.html.
"""


class FoodMenuView(generic.ListView):
	# the queryset for getting all food with the availability status of true
	queryset = Food.objects.filter(food_availability=True)
	# tell the html template to only show 12 food maximum per-page and pyt the rest in the next and previous
	paginate_by = 12
	# our html template that would show all the available food
	template_name = 'order/_food_menu.html'


"""
this view is the first page to be showned when
the user or customer visit out fast food application
"""


def index_page(request):
	# create an empty dictionary that would hold some variable to be used in the html
	context_dict = {}
	# since our index page has a form that accept customer tel_number
	# let check if the customer make a post request to initiate an order
	# if it is a post then we process what is inside the form basically just the tel_number
	if request.method == 'POST':
		# let get the form values from the customer form
		customer_form = CustomerForm(data=request.POST)
		# is the form valid(tgat means if the customer has actually fill the form and the form contain some info
		if customer_form.is_valid() and customer_form.is_bound:
			# get the form data from the customer form
			form_data = customer_form.data
			# we just need only the customer tel_number as his order number and store it inside the
			# customer_tel_number variable
			customer_tel_number = form_data['tel_number']
			# first let check if the customer telephone number is already registered in our db
			# so as not allow two different number but need better way to go around it ie (FUTURE ENHANCEMENT)
			is_customer_already_registered = None  # assuming the customer is not registered

			try:
				# let check by getting the customer by passing the tel_number from the form as a criteria
				# and store it inside the is_custoer_already_registered variable
				is_customer_already_registered = Customer.objects.get(tel_number=customer_tel_number)
			except Customer.DoesNotExist:
				# incase the customer with this particular tel_number does not exits we should
				# prevent some error and continue to the else block after the
				pass
			# if the customer is already registered ok we just need to logged them into the system
			# and not register them again
			if is_customer_already_registered:
				# we authenticated the customer by using the tel_number as their username
				# and default as their password for all customer
				user = authenticate(username=customer_tel_number, password='default')
				# if everything is succesfull logged them in
				if user:
					login(request, user)

					a_cart = None
					# check if the customer has a cart
					try:
						a_cart = Cart.objects.get(customer=is_customer_already_registered)
					except Cart.DoesNotExist:
						pass

					# let check if the customer cart is not active
					if a_cart and not a_cart.active:
						try:
							#if it is not let delete all the item in the cart
							orders = FoodOrder.objects.filter(cart=a_cart)
							orders.delete()
						except FoodOrder.DoesNotExist:
							pass
						# then make the cart active
						a_cart.active = True
						a_cart.save()
			else:
				# eles the customer is not in our system let automatically registered them as
				# a new user and a customer in our system
				a_user = User.objects.create_user(username=customer_tel_number, password="default")
				a_customer = Customer.objects.create(
					user=a_user,
					tel_number=customer_tel_number
				)
				# if all the database operation for saving the customer is sucessfull then we
				# automatically logged the customer into the system
				if a_customer:
					user = authenticate(username=customer_tel_number, password='default')
					login(request, user)
		# if everything was ok then we redirect the user to the menu list to see all food
		return HttpResponseRedirect('/fast/menu')
	else:
		# ok it is a get method so we show the customer form on the index page
		customer_form = CustomerForm()
	# add the customer form object to the dictionary to be passed to the template for processing
	context_dict['customer_form'] = customer_form
	# return every thing including the dictionary
	return render(request, template_name='order/_index.html', context=context_dict)


"""
this view is responsible for adding a food
from the menu to the order/cart and also increasing
the particular food quantity
Remember this view recieve a AJAX call
"""


def add_item_to_cart(request):
	# we are sending an AJAX (javascript get call to this view) with the food_id
	# to be added passed to it. Get the food_id from the request
	food_id = request.GET['food_id']
	# assuming the current_custoer cart is empty
	current_cart = None
	try:
		# check does the food the customer want to add to his/her
		# order exits
		food_to_add = Food.objects.get(pk=food_id)
	except Food.DoesNotExist:
		# remeber we are keeping this in check of any error
		pass
	else:
		# ok no error found that food actually exits
		try:
			# let track the customer who is making this request by getting the
			# customer information by passing the request.user as a query for
			# getting the current_customer
			current_customer = Customer.objects.get(user=request.user)
			# get the customer cart if  he has any and it is active
			cart = Cart.objects.get(customer=current_customer, active=True)
		except Cart.DoesNotExist, Customer.DoesNotExist:
			# check whether it does not exits which if the cart does not
			# create it and assing the customer as a foriegn key to the cart
			current_customer = Customer.objects.get(user=request.user)
			# cretae the cart for the current customer
			cart = Cart.objects.create(
				customer=current_customer
			)
			# save the cart
			cart.save()
			pass
		# add the food to the cart
		cart.add_to_cart(food_id)

		current_cart = cart

	# get all order item that are in the current cart  by getting it through the filter
	ordered_item = FoodOrder.objects.filter(cart=current_cart)
	# return the count of the ordered items
	print "Increasing Items now is ", ordered_item.count()
	return HttpResponse(ordered_item.count())


# remove a food from the carts by decreasing a particular order
def remove_food_from_cart(request):
	current_cart = None
	# the food id from the javascript request
	food_id = request.GET['food_id']
	try:
		food_to_remove = Food.objects.get(pk=food_id)
	except Food.DoesNotExist:
		pass
	else:
		current_customer = Customer.objects.get(user=request.user)
		cart = Cart.objects.get(customer=current_customer, active=True)
		current_cart = cart
		cart.remove_from_cart(food_id)

	ordered_item = FoodOrder.objects.filter(cart=current_cart)
	print "Removing Items now is ", ordered_item.count()
	return HttpResponse(ordered_item.count())


# when the customer is ready to pay
@login_required(login_url='/fast/')
def get_food_from_cart(request):
	context_dict = {}

	current_customer = Customer.objects.get(user=request.user)

	try:
		cart = Cart.objects.get(customer=current_customer)
	except Cart.DoesNotExist:
		return HttpResponseRedirect('/fast/menu/')
	if not cart.active:
		return HttpResponseRedirect('/fast/menu/')
	orders = FoodOrder.objects.filter(cart=cart)
	total = 0
	count = 0
	food_items = []

	for order in orders:
		food_items.append(order.food)
		total += (order.food.food_price * order.quantity)
		count += order.quantity
	if request.method == 'POST':
		payment_form = PaymentForm(request.POST)
		if payment_form.is_valid() and payment_form.is_bound:
			# payment details where correct
			payment_information = payment_form.data
			card_name = payment_information['card_name']
			card_number = payment_information['card_number']
			new_customer_payment = Payment.objects.create(
				cart=cart,
				card_name=card_name,
				card_number=card_number,
				total=total,
				payed=True
			)
			cart.active = False
			cart.save()
			logout(request)
		return HttpResponseRedirect(redirect_to='/fast/payment-successful/')
	else:
		# payment details was not correct
		payment_form = PaymentForm()

	context_dict['payment_form'] = payment_form
	context_dict['total'] = total
	context_dict['count'] = count
	context_dict['food_list'] = food_items
	context_dict['orders'] = orders

	return render(request, template_name='order/_view_cart.html', context=context_dict)


def success_full_payment(request):
	return render(request, template_name='order/_successfull_payment.html', context={})


# the function that logs the customer out of the system
# the user must be logged in before
@login_required(login_url='/fast/')
def user_logout(request):
	logout(request)
	return HttpResponseRedirect('/fast/')


def handler404(request):
	response = render_to_response('order/404.html', {}, context_instance=RequestContext(request))
	response.status_code = 404
	return response


def handler500(request):
	response = render_to_response('order/500.html', {}, context_instance=RequestContext(request))
	response.status_code = 500
	return response
