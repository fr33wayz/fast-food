$(document).ready(function () {
    $('.button-collapse').sideNav();
    $('select').material_select();
    $('.parallax').parallax();
    $('ul.tabs').tabs();


    var elements = document.getElementsByClassName('card-action');

    for (var i = 0; i < elements.length; i++) { //for each one
        var element = elements[i];
        var addToCartButton = element.getElementsByClassName('btn-floating')[0];
        //when the button is clicked
        addToCartButton.onclick = function () {
            var food_to_add_to_cart_id = $(this).attr("data-food");
            $.get('/fast/add-item/', {food_id: food_to_add_to_cart_id}, function (data) {
                $('#cart_item').html(data);
            });
            $(this).attr('class', 'btn-floating disabled');
        }
    }


    (function () {
        var addRemoveButtonElements = document.getElementsByClassName('add-item');
        for (var i = 0; i < addRemoveButtonElements.length; i++) { //for each one
            var btnElement = addRemoveButtonElements[i];
            var increaseItem = btnElement.getElementsByClassName('btn-floating')[0];
            //when the button is clicked
            increaseItem.onclick = function () {
                var food_item_to_increase = $(this).attr("data-food");
                $.get('/fast/add-item/', {food_id: food_item_to_increase}, function (data) {
                    $('#cart_item').html(data);
                }).done(function () {
                    window.location.reload();
                });

            }
        }
    })();

    (function () {
        var removeItemBtnElement = document.getElementsByClassName('remove-item');
        for (var i = 0; i < removeItemBtnElement.length; i++) { //for each one
            var btnElement = removeItemBtnElement[i];
            var itemToRemove = btnElement.getElementsByClassName('btn-floating')[0];
            //when the button is clicked
            itemToRemove.onclick = function () {
                var food_to_remove_id = $(this).attr("data-food");
                $.get('/fast/remove-item/', {food_id: food_to_remove_id}, function (data) {
                    $('#cart_item').html(data);
                }).done(function () {
                    window.location.reload();
                });
            }
        }
    })()

});

function makePayment(event) {
    event.preventDefault();

}